function writeDate(){
    document.write(Date());
}

function getBrowserInfo(){
    let userAgent = navigator.userAgent;
    let browserName;
    if(userAgent.match(/chrome|chromium|crios/i)){
        browserName = "chrome";
    } else if (userAgent.match(/firefox|fxios/i)){
        browserName = "firefox";
    } else if (userAgent.match(/safari/i)){
        browserName = "safari";
    } else if (userAgent.match(/opr\//i)){
        browserName = "opera";
    } else if (userAgent.match(/edg/i)){
        browserName = "edge";
    }else{
        browserName="No browser detection";
    }
    document.write(browserName);
}

function submitPressed(){
    let input = document.getElementById("inputField1").value;
    document.getElementById("p1").innerHTML = "Hello " + input + ".";
}

function updateText(){
    let realTimeText = document.getElementById("inputField2").value;
    document.getElementById("p2").innerHTML = realTimeText;
}

function mouseEnterDuck(){
    document.getElementById("imageResponse").innerHTML = "Quack";
}

function mouseLeaveDuck(){
    document.getElementById("imageResponse").innerHTML = "";
}

function boldText(x) { 
    x.style.fontWeight='bold'; 
} 
function normalText(x) { 
    x.style.fontWeight='normal'; 
} 
